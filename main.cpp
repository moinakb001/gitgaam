#define VOLK_IMPLEMENTATION
#include <volk.h>
#include <stdint.h>
#include <vulkan/vulkan.h>
#include <dlfcn.h>
#include <stdio.h>
#include "types.hpp"
#include "vk.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

int main()
{
  SDL_Event evt;
  SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
  Vulkan::Init();
  auto wnd = SDL_CreateWindow("Application", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 400, 400,
                                     SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);

  Vulkan::InitWindow(wnd);
  bool quit = false;
  while (!quit){
    while (SDL_PollEvent(&evt)){
      printf("HI %u\n",  evt.type);
      if(evt.type == SDL_WINDOWEVENT)
      {
        int x, y;
        SDL_Vulkan_GetDrawableSize(wnd, &x, &y);
        printf("Win %u  - %u %u\n",  evt.window.event, x, y);
      }
        if (evt.type == SDL_QUIT){
            quit = true;
        }
        if (evt.type == SDL_KEYDOWN){
            quit = true;
        }
        if (evt.type == SDL_MOUSEBUTTONDOWN){
            quit = true;
        }
    }
}
  Vulkan::Finish();

  return 0;
}
