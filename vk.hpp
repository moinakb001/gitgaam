#pragma once
#include <dlfcn.h>
#include <vulkan/vulkan.h>
#include <volk.h>
#include <wayland-client.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include "types.hpp"

#define VAR_ALLOC_MULT(fn, num, ptr, ...) \
    num = 0; \
    fn(__VA_ARGS__, &(num), NULL); \
    (ptr) = (typeof(ptr)) __builtin_alloca(sizeof((ptr)[0]) * (num)); \
    fn(__VA_ARGS__, &(num), ptr);

namespace Vulkan
{
    VkInstance inst{};
    VkSurfaceKHR surf{};
    static inline void Init()
    {
        SDL_Vulkan_LoadLibrary("libs/libvulkan.so");
        PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)  SDL_Vulkan_GetVkGetInstanceProcAddr();
        volkInitializeCustom(vkGetInstanceProcAddr);
    }
    static inline void InitDevices()
    {
        uint32_t num = 0;
        VkPhysicalDevice *devs = NULL;
        VAR_ALLOC_MULT(vkEnumeratePhysicalDevices, num, devs, inst);
        for(uint32_t i = 0; i < num; i++)
        {
            uint32_t numQueues, numExts;
            VkQueueFamilyProperties *pFams;
            VkExtensionProperties *pProps;
            VkPhysicalDeviceProperties deviceProperties;
            VkPhysicalDeviceFeatures deviceFeatures;
            VAR_ALLOC_MULT(vkGetPhysicalDeviceQueueFamilyProperties,  numQueues, pFams, devs[i]);
            VAR_ALLOC_MULT(vkEnumerateDeviceExtensionProperties, numExts, pProps, devs[i], NULL);
            vkGetPhysicalDeviceProperties(devs[i], &deviceProperties);
            vkGetPhysicalDeviceFeatures(devs[i], &deviceFeatures);
            printf("%s - %u\n",  deviceProperties.deviceName,  deviceProperties.deviceType);
            for (uint32_t j = 0; j < numQueues; j++)
            {
                printf("family %u - 0x%x %u\n", j, pFams[j].queueFlags, pFams[j].queueCount);
            }
            for (uint32_t j = 0; j < numExts; j++)
            {
                printf("ext %u - %s\n", j, pProps[j].extensionName);
            }
        }


    }
    static inline void InitWindow(SDL_Window *pWnd)
    {
        VkInstanceCreateInfo inf{};
        VkApplicationInfo appInf{};
        const char **ppStore;
        appInf.sType =  VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInf.apiVersion = VK_API_VERSION_1_0;
        inf.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        inf.pApplicationInfo = &appInf;
        VAR_ALLOC_MULT(SDL_Vulkan_GetInstanceExtensions, inf.enabledExtensionCount, ppStore, pWnd);
        inf.ppEnabledExtensionNames = ppStore;
        vkCreateInstance(&inf, NULL, &inst);
        volkLoadInstance(inst);
        SDL_Vulkan_CreateSurface(pWnd, inst, &surf);
        InitDevices();
    }
    
    static inline void Finish()
    {
        volkFinalize();
        SDL_Vulkan_UnloadLibrary();
    }
};